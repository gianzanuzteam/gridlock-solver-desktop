/*************************************************************************************
* Includes
*************************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <conio.h>
#include <stdlib.h>
#include "gridlock.h"

/*************************************************************************************
* Private macros
*************************************************************************************/
#define NUMBER_OF_PIECES  25

/*************************************************************************************
* Private typedefs
*************************************************************************************/

/*************************************************************************************
* Private variables
*************************************************************************************/

/*************************************************************************************
* Private prototypes
*************************************************************************************/
void printMatrix(int row_arg, int col_arg, int piece[row_arg][col_arg]);

/*******************************************************************************
   main
*****************************************************************************//*
 * @brief The main function.
 * @param void
 * @return TRUE Solution found.
           FALSE Otherwise.
*******************************************************************************/
int main()
{
    /* Limpeza de variáveis */
    piece_info_t pieceInfo[NUMBER_OF_PIECES];
    memset(pieceInfo, 0, sizeof(pieceInfo));

    printf("Inicializando ferramenta de solucao do jogo 'Cilada'\r\n");

    int numberOfPieces;
    for(numberOfPieces = 0; numberOfPieces < NUMBER_OF_PIECES; numberOfPieces++)
    {
        char piece;
        printf("Digite a peca %d: ", numberOfPieces+1);
        piece = getch();
        if(piece == '\r' || piece == '\n')
        {
            printf("\r\n");
            break;
        }

        /* Coloca em UPPER CASE */
        piece = toupper(piece);
        if(piece < 'A' || piece > 'N')
        {
            printf("Peca invalida\r\n");
            numberOfPieces--;
            continue;
        }
        else
        {
            printf("%c\r\n", piece);
            pieceInfo[numberOfPieces].piece = (pieces_t) (piece - 'A');
        }
    }
    printf("Finalizando entrada de pecas, procurando solucao...\r\n");
    printf("\r\n");

    piece_stats_t stats;
    if(solve(numberOfPieces, pieceInfo, &stats))
    {
        printf("SOLUCAO ENCONTRADA APOS %lu INTERACOES, %lu INSERCOES E %lu REMOCOES!\n", stats.interractions, stats.inserts, stats.removes);
        printMatrix(MATRIX_ROW_SIZE + 1, MATRIX_COL_SIZE + 1, matrix);
        system("pause");
        return true;
    }

    printf("SOLUCAO NAO ENCONTRADA APOS %lu INTERACOES, %lu INSERCOES E %lu REMOCOES!\n", stats.interractions, stats.inserts, stats.removes);
    system("pause");
    return false;
}

/*******************************************************************************
   printMatrix
*****************************************************************************//*
 * @brief
 * @param
 * @return void.
*******************************************************************************/
void printMatrix(int row_arg, int col_arg, int piece[row_arg][col_arg])
{
    for(int row = 0; row < row_arg - 1; row++)
    {
        for(int col = 0; col < col_arg - 1; col++)
        {
            printf("%2d ", piece[row][col]);
        }
        printf("\n");
    }
    printf("\n");
}
