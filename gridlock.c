/*************************************************************************************
* Includes
*************************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "gridlock.h"

/*************************************************************************************
* Private macros
*************************************************************************************/

/*************************************************************************************
* Private variables
*************************************************************************************/
static piece_stats_t statistics = { 0 };

/*************************************************************************************
* Private prototypes
*************************************************************************************/
bool checkPosition (int row, int col, int piece[PIECE_ROW_SIZE][PIECE_COL_SIZE]);
bool findPosition(int* rot_arg, int* row_arg, int* col_arg, const int* piecePtr);

/*******************************************************************************
   main
*****************************************************************************//*
 * @brief The main function.
 * @param void
 * @return TRUE Solution found.
           FALSE Otherwise.
*******************************************************************************/
int solve(int amount, piece_info_t* pieceInfo, piece_stats_t* stats)
{
    /* Limpeza de variáveis */
    memset(&statistics, 0, sizeof(statistics));
    memset(matrix, 0, sizeof(matrix));

    /* Valores iniciais */
    for(int i=0; i<amount; i++)
    {
        pieceInfo[i].row = 0;
        pieceInfo[i].col = 0;
        pieceInfo[i].rot = 0;
    }

    /* Para cada peça */
    for(int pieceCount=0; pieceCount<amount; pieceCount++)
    {
        /* Procura uma prosição na matrix */
        if(findPosition(&pieceInfo[pieceCount].rot, &pieceInfo[pieceCount].row, &pieceInfo[pieceCount].col, (const int*) &PIECES[pieceInfo[pieceCount].piece]))
        {
            /* STATS: Incrementa nova inserção da peça */
            statistics.inserts++;

            /* Obtém combinação da peça */
            int piece[PIECE_ROW_SIZE][PIECE_COL_SIZE];
            memcpy(piece, (const int*) &PIECES[pieceInfo[pieceCount].piece] + pieceInfo[pieceCount].rot*(PIECE_ROW_SIZE*PIECE_COL_SIZE), sizeof(piece));

            /* Marca peça como ocupada */
            if(piece[0][0] != NONE__)
                matrix[pieceInfo[pieceCount].row][pieceInfo[pieceCount].col] = pieceCount + 1;
            if(piece[1][0] != NONE__)
                matrix[pieceInfo[pieceCount].row+1][pieceInfo[pieceCount].col] = pieceCount + 1;
            if(piece[0][1] != NONE__)
                matrix[pieceInfo[pieceCount].row][pieceInfo[pieceCount].col+1] = pieceCount + 1;
            if(piece[1][1] != NONE__)
                matrix[pieceInfo[pieceCount].row+1][pieceInfo[pieceCount].col+1] = pieceCount + 1;

            continue;
        }
        else
        {
            /* STATS: Peça removida da matrix */
            statistics.removes++;

            /* Reset nas configurações da peça atual */
            pieceInfo[pieceCount].rot = 0;
            pieceInfo[pieceCount].row = 0;
            pieceInfo[pieceCount].col = 0;

            /* Retorna para a peça anterior */
            pieceCount--;
            if(pieceCount < 0)
            {
                /* Solução não encontrada */
                if(stats != NULL)
                    memcpy(stats, &statistics, sizeof(statistics));
                return false;
            }

            /* Obtém a combinação da peça anterior */
            int piece[PIECE_ROW_SIZE][PIECE_COL_SIZE];
            memcpy(piece, (const int*) &PIECES[pieceInfo[pieceCount].piece] + pieceInfo[pieceCount].rot*(PIECE_ROW_SIZE*PIECE_COL_SIZE), sizeof(piece));

            /* Remove peça da matrix, marcando como livre */
            if(piece[0][0] != NONE__)
                matrix[pieceInfo[pieceCount].row][pieceInfo[pieceCount].col] = EMPTY_SPACE;
            if(piece[1][0] != NONE__)
                matrix[pieceInfo[pieceCount].row+1][pieceInfo[pieceCount].col] = EMPTY_SPACE;
            if(piece[0][1] != NONE__)
                matrix[pieceInfo[pieceCount].row][pieceInfo[pieceCount].col+1] = EMPTY_SPACE;
            if(piece[1][1] != NONE__)
                matrix[pieceInfo[pieceCount].row+1][pieceInfo[pieceCount].col+1] = EMPTY_SPACE;

            /* Compensa o acrescimo dado pelo loop for para a próxima interação */
            pieceCount--;
            continue;
        }
    }

    /* Solução encontrada */
    if(stats != NULL)
        memcpy(stats, &statistics, sizeof(statistics));
    return true;
}

/*******************************************************************************
   findPosition
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
bool findPosition(int* rot_arg, int* row_arg, int* col_arg, const int* piecePtr)
{
    /* Cria um identificador único da combinação anterior, com base na rotação, linha e coluna */
    int lastUniqueCombination = (*rot_arg)*256 + (*row_arg)*16 + (*col_arg);

    /* Para cada possível ROTAÇÃO da peça */
    for(int rot = 0; rot < PIECE_ROT_SIZE; rot++)
    {
        /* Coloca a peça em uma matrix */
        int piece[PIECE_ROW_SIZE][PIECE_COL_SIZE];
        memcpy(piece, piecePtr + rot*(PIECE_ROW_SIZE*PIECE_COL_SIZE), sizeof(piece));

        /* Para cada LINHA */
        for(int row = 0; row < MATRIX_ROW_SIZE; row++)
        {
            /* Para cada COLUNA */
            for(int col = 0; col < MATRIX_COL_SIZE; col++)
            {
                /* Testa se a combinação é possível */
                if(checkPosition (row, col, piece))
                {
                    /* Verifica se a combinação atual já foi utilizada anteriormente, com base no identificador único */
                    int currentUniqueCombination = 256*rot + 16*row + col;
                    if(lastUniqueCombination >= currentUniqueCombination)
                        continue;

                    /* Guarda a nova combinação encontrada */
                    *rot_arg = rot;
                    *row_arg = row;
                    *col_arg = col;

                    return true;
                }
            }
        }
    }

    /* Nenhuma posição possível encontrada */
    return false;
}

/*******************************************************************************
   checkPosition
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
bool checkPosition (int row, int col, int piece[PIECE_ROW_SIZE][PIECE_COL_SIZE])
{
    /* STATS: Nova interação */
    statistics.interractions++;

    /* Posição 0-0 */
    if(piece[0][0] != NONE__)
    {
        /* Verifica se é a borda */
        if(referenceMatrix[row][col] == BORDER)
            return false;
        /* Verifica se está ocupado */
        if(matrix[row][col] != EMPTY_SPACE)
            return false;
        /* Verifica se é o simbolo correto */
        if(piece[0][0] != referenceMatrix[row][col])
            return false;
    }

    /* Posição 1-0 */
    if(piece[1][0] != NONE__)
    {
        /* Verifica se é a borda */
        if(referenceMatrix[row+1][col] == BORDER)
            return false;
        /* Verifica se está ocupado */
        if(matrix[row+1][col] != EMPTY_SPACE)
            return false;
        /* Verifica se é o simbolo correto */
        if(piece[1][0] != referenceMatrix[row+1][col])
            return false;
    }

    /* Posição 0-1 */
    if(piece[0][1] != NONE__)
    {
        /* Verifica se é a borda */
        if(referenceMatrix[row][col+1] == BORDER)
            return false;
        /* Verifica se está ocupado */
        if(matrix[row][col+1] != EMPTY_SPACE)
            return false;
        /* Verifica se é o simbolo correto */
        if(piece[0][1] != referenceMatrix[row][col+1])
            return false;
    }

    /* Posição 1-1 */
    if(piece[1][1] != NONE__)
    {
        /* Verifica se é a borda */
        if(referenceMatrix[row+1][col+1] == BORDER)
            return false;
        /* Verifica se está ocupado */
        if(matrix[row+1][col+1] != EMPTY_SPACE)
            return false;
        /* Verifica se é o simbolo correto */
        if(piece[1][1] != referenceMatrix[row+1][col+1])
            return false;
    }

    /* Posição foi encontrada */
    return true;
}
