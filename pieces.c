/*************************************************************************************
* Includes
*************************************************************************************/
#include "pieces.h"

/*************************************************************************************
* Private variables
*************************************************************************************/
const int PIECES[PIECE_AMOUNT][PIECE_ROT_SIZE][PIECE_ROW_SIZE][PIECE_COL_SIZE] = {
    /* A */
    {
        {
            { CROSS_, CIRCLE },
            { NONE__, NONE__ },
        },
        {
            { CROSS_, NONE__ },
            { CIRCLE, NONE__ },
        },
        {
            { CIRCLE, CROSS_ },
            { NONE__, NONE__ },
        },
        {
            { CIRCLE, NONE__ },
            { CROSS_, NONE__ },
        },
    },
    /* B */
    {
        {
            { CROSS_, SQUARE },
            { NONE__, NONE__ },
        },
        {
            { CROSS_, NONE__ },
            { SQUARE, NONE__ },
        },
        {
            { SQUARE, CROSS_ },
            { NONE__, NONE__ },
        },
        {
            { SQUARE, NONE__ },
            { CROSS_, NONE__ },
        },
    },
    /* C */
    {
        {
            { SQUARE, CIRCLE },
            { NONE__, NONE__ },
        },
        {
            { SQUARE, NONE__ },
            { CIRCLE, NONE__ },
        },
        {
            { CIRCLE, SQUARE },
            { NONE__, NONE__ },
        },
        {
            { CIRCLE, NONE__ },
            { SQUARE, NONE__ },
        },
    },
    /* D */
    {
        {
            { CROSS_, CROSS_ },
            { NONE__, NONE__ },
        },
        {
            { CROSS_, NONE__ },
            { CROSS_, NONE__ },
        },
        {
            { CROSS_, CROSS_ },
            { NONE__, NONE__ },
        },
        {
            { CROSS_, NONE__ },
            { CROSS_, NONE__ },
        },
    },
    /* E */
    {
        {
            { SQUARE, SQUARE },
            { NONE__, NONE__ },
        },
        {
            { SQUARE, NONE__ },
            { SQUARE, NONE__ },
        },
        {
            { SQUARE, SQUARE },
            { NONE__, NONE__ },
        },
        {
            { SQUARE, NONE__ },
            { SQUARE, NONE__ },
        },
    },
    /* F */
    {
        {
            { CIRCLE, CIRCLE },
            { NONE__, NONE__ },
        },
        {
            { CIRCLE, NONE__ },
            { CIRCLE, NONE__ },
        },
        {
            { CIRCLE, CIRCLE },
            { NONE__, NONE__ },
        },
        {
            { CIRCLE, NONE__ },
            { CIRCLE, NONE__ },
        },
    },
    /* G */
    {
        {
            { SQUARE, CIRCLE },
            { SQUARE, NONE__ },
        },
        {
            { SQUARE, SQUARE },
            { NONE__, CIRCLE },
        },
        {
            { NONE__, SQUARE },
            { CIRCLE, SQUARE },
        },
        {
            { CIRCLE, NONE__ },
            { SQUARE, SQUARE },
        },
    },
    /* H */
    {
        {
            { SQUARE, CROSS_ },
            { NONE__, CIRCLE },
        },
        {
            { NONE__, SQUARE },
            { CIRCLE, CROSS_ },
        },
        {
            { CIRCLE, NONE__ },
            { CROSS_, SQUARE },
        },
        {
            { CROSS_, CIRCLE },
            { SQUARE, NONE__ },
        },
    },
    /* I */
    {
        {
            { CIRCLE, SQUARE },
            { CROSS_, NONE__ },
        },
        {
            { CROSS_, CIRCLE },
            { NONE__, SQUARE },
        },
        {
            { NONE__, CROSS_ },
            { SQUARE, CIRCLE },
        },
        {
            { SQUARE, NONE__ },
            { CIRCLE, CROSS_ },
        },
    },
    /* J */
    {
        {
            { CROSS_, CROSS_ },
            { SQUARE, NONE__ },
        },
        {
            { SQUARE, CROSS_ },
            { NONE__, CROSS_ },
        },
        {
            { NONE__, SQUARE },
            { CROSS_, CROSS_ },
        },
        {
            { CROSS_, NONE__ },
            { CROSS_, SQUARE },
        },
    },
    /* K */
    {
        {
            { CIRCLE, CIRCLE },
            { SQUARE, NONE__ },
        },
        {
            { SQUARE, CIRCLE },
            { NONE__, CIRCLE },
        },
        {
            { NONE__, SQUARE },
            { CIRCLE, CIRCLE },
        },
        {
            { CIRCLE, NONE__ },
            { CIRCLE, SQUARE },
        },
    },
    /* L  */
    {
        {
            { CIRCLE, SQUARE },
            { CIRCLE, NONE__ },
        },
        {
            { CIRCLE, CIRCLE },
            { NONE__, SQUARE },
        },
        {
            { NONE__, CIRCLE },
            { SQUARE, CIRCLE },
        },
        {
            { SQUARE, NONE__ },
            { CIRCLE, CIRCLE },
        },
    },
    /* M */
    {
        {
            { CROSS_, SQUARE },
            { CIRCLE, NONE__ },
        },
        {
            { CIRCLE, CROSS_ },
            { NONE__, SQUARE },
        },
        {
            { NONE__, CIRCLE },
            { SQUARE, CROSS_ },
        },
        {
            { SQUARE, NONE__ },
            { CROSS_, CIRCLE },
        },
    },
    /* N */
    {
        {
            { SQUARE, CROSS_ },
            { CIRCLE, NONE__ },
        },
        {
            { CIRCLE, SQUARE },
            { NONE__, CROSS_ },
        },
        {
            { NONE__, CIRCLE },
            { CROSS_, SQUARE },
        },
        {
            { CROSS_, NONE__ },
            { SQUARE, CIRCLE },
        },
    },
};

/* Reference board matrix */
const int referenceMatrix[MATRIX_ROW_SIZE + 1][MATRIX_COL_SIZE + 1] = {
    { CIRCLE, CIRCLE, CROSS_, CROSS_, CROSS_, CIRCLE, SQUARE, BORDER },
    { CIRCLE, SQUARE, SQUARE, SQUARE, CROSS_, SQUARE, CROSS_, BORDER },
    { SQUARE, CIRCLE, CROSS_, CIRCLE, CIRCLE, SQUARE, CIRCLE, BORDER },
    { SQUARE, CIRCLE, CROSS_, SQUARE, CROSS_, CROSS_, CIRCLE, BORDER },
    { BORDER, BORDER, BORDER, BORDER, BORDER, BORDER, BORDER, BORDER },
};

/* Matrix utilizada para inserção das peças */
int matrix[MATRIX_ROW_SIZE + 1][MATRIX_COL_SIZE + 1] = { 0 };
