#ifndef GRIDLOCK_H
#define GRIDLOCK_H

#ifdef __cplusplus
extern "C" {
#endif

/*************************************************************************************
* Includes
*************************************************************************************/
#include <ctype.h>
#include "pieces.h"

/*************************************************************************************
* Public macros
*************************************************************************************/

/*************************************************************************************
* Public type definitions
*************************************************************************************/
typedef struct piece_info_tag {
    int         rot;
    int         row;
    int         col;
    pieces_t    piece;
} piece_info_t;

/* Estatísticas */
typedef struct piece_stats_tag {
    unsigned long int interractions;
    unsigned long int inserts;
    unsigned long int removes;
} piece_stats_t;

/*************************************************************************************
* Public prototypes
*************************************************************************************/
int solve(int amount, piece_info_t* pieceInfo, piece_stats_t* stats);

#ifdef __cplusplus
}
#endif

#endif // GRIDLOCK_H
