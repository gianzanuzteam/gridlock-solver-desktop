#ifndef PIECES_H
#define PIECES_H

#ifdef __cplusplus
extern "C" {
#endif

/*************************************************************************************
* Includes
*************************************************************************************/
#include <ctype.h>

/*************************************************************************************
* Public macros
*************************************************************************************/
/* Pieces definitions */
#define PIECE_ROW_SIZE    2
#define PIECE_COL_SIZE    2
#define PIECE_ROT_SIZE    4
#define PIECE_AMOUNT      14

/* Matrix definitions */
#define MATRIX_ROW_SIZE   4
#define MATRIX_COL_SIZE   7
#define EMPTY_SPACE       0

/*************************************************************************************
* Public type definitions
*************************************************************************************/
/* Shapes */
typedef enum {
    BORDER      = 0,
    CIRCLE,
    SQUARE,
    CROSS_,
    NONE__,
} shapes_t;

/* Pieces names */
typedef enum pieces_tag {
    A           = 0,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
} pieces_t;

/*************************************************************************************
* External variables
*************************************************************************************/
extern const int    PIECES[PIECE_AMOUNT][PIECE_ROT_SIZE][PIECE_ROW_SIZE][PIECE_COL_SIZE];
extern const int    referenceMatrix[MATRIX_ROW_SIZE + 1][MATRIX_COL_SIZE + 1];
extern int          matrix[MATRIX_ROW_SIZE + 1][MATRIX_COL_SIZE + 1];

#ifdef __cplusplus
}
#endif

#endif // PIECES_H
